<?php 
return [
    'back' => 'back',
    'back_to_home' => 'back_to_home',
    'page' => [
        'not_found' => 'page.not_found',
    ],
    'permission' => 'El usuario no tiene el permiso',
    'store_disabled' => 'store_disabled',
];