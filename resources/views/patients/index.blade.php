@extends('layouts.app', ['activePage' => 'patients', 'titlePage' => 'Lista de pacientes'])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Pacientes</h4>
                        <p class="card-category"> Aqui puedes administrar tus pacientes</p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12 _text-right">
                                <a href="{{route('patient.create')}}" class="btn btn-sm btn-primary">Agregar paciente</a>
                            </div>
                            <div class="col-md-6 col-xs-12 float-right">
                                @include('patients.search')
                            </div>
                        </div>
                        @if (session('status'))
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span>{{ session('status') }}</span>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>Imagen</th>
                                    <th>
                                        Apellido Paterno
                                    </th>
                                    <th>
                                        Apellido Materno
                                    </th>
                                    <th>
                                        Nombre (s)
                                    </th>
                                    <th>Edad</th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Teléfono
                                    </th>
                                    <th>
                                        Dirección
                                    </th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach ($patients as $patient)
                                    <tr>
                                        <td>
                                            <img class="img-circle elevation-2" style="width:32px" src="{{$patient->getFirstMediaUrl('image', 'thumb')}}" alt="default">
                                        </td>
                                        <td>{{$patient->last_name}}</td>
                                        <td>{{$patient->mother_last_name}}</td>
                                        <td>{{$patient->name}}</td>
                                        <td>{{$patient->age}}</td>
                                        <td>{{$patient->email}}</td>
                                        <td>{{$patient->phone}}</td>
                                        <td>{{$patient->address}}</td>
                                        <td class="td-actions text-right">
                                            @can('patient.evolution')
                                            <a rel="tooltip" class="btn btn-info btn-link" href="{{ route('patient.show', $patient->id) }}" data-original-title="" title="Evolución">
                                                <i class="material-icons">note_add</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                            @endcan
                                            @if(empty($patient->deleted_at))
                                            @can('patient.edit')
                                            <a rel="tooltip" class="btn btn-info btn-link" href="{{ route('patient.edit', $patient->id) }}" data-original-title="" title="Editar">
                                                <i class="material-icons">edit</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                            @endcan
                                            @can('patient.destroy')
                                            <a rel="tooltip" class="btn btn-warning btn-link" href="" data-target="#modal-delete-{{$patient->id}}" data-toggle="modal" title="Baja">
                                                <i class="material-icons">close</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                            @endcan
                                            @else
                                            @can('patient.active')                                           
                                            <a rel="tooltip" class="btn btn-warning btn-link" href="" data-target="#modal-delete-{{$patient->id}}" data-toggle="modal" title="Activar">
                                                <i class="material-icons">done_all</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                            @endcan
                                            @endif
                                        </td>
                                    </tr>
                                    @include('patients.modal')
                                    @endforeach
                                </tbody>
                            </table>
                            {!!$patients->render()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection