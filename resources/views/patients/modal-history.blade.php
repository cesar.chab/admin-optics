<div class="modal fade modal-slide-in-right" id="modal-history-{{$patient->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar evolución </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            {{Form::Open(array('id' => 'saveHistory', 'autocomplete' => 'off', 'action'=>array('HistoryController@store'),'method'=>'post'))}}
            <div class="modal-body">
                <input type="hidden" name="patient_id" value="{{$patient->id}}">
                <div class="col-12">
                    <div class="form-group bmd-form-group">
                        <label for="title" class="bmd-label-floating"> Titulo </label>
                        <input type="text" class="form-control" name="title" id="title" maxlength="100">
                    </div>
                    <div class="form-group bmd-form-group">
                        <label for="right_eye" class="bmd-label-floating"> OD</label>
                        <input type="text" class="form-control" name="right_eye" id="right_eye" maxlength="100">
                    </div>
                    <div class="form-group bmd-form-group">
                        <label for="left_eye" class="bmd-label-floating"> OI</label>
                        <input type="text" class="form-control" name="left_eye" id="left_eye" maxlength="100">
                    </div>
                    <div class="form-group bmd-form-group">
                        <label for="right_rise" class="bmd-label-floating"> AD </label>
                        <input type="text" class="form-control" name="right_rise" id="right_rise" maxlength="100">
                    </div>
                    <div class="form-group bmd-form-group">
                        <label for="left_rise" class="bmd-label-floating"> DI </label>
                        <input type="text" class="form-control" name="left_rise" id="left_rise" maxlength="100">
                    </div>
                    <div class="form-group bmd-form-group">
                        <label for="lens_type" class="bmd-label-floating"> Tipo de lente </label>
                        <input type="text" class="form-control" name="lens_type" id="lens_type" maxlength="100">
                    </div>
                    <div class="form-group bmd-form-group">
                        <label for="comments" class="bmd-label-floating"> Comentarios *</label>
                        <textarea class="form-control" name="comments" id="comments" cols="30" rows="5" required="true" aria-required="true" aria-invalid="true"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="btn-save-history" class="btn btn-success">Guardar</button>
                </div>
                {{Form::Close()}}

            </div>
        </div>
    </div>