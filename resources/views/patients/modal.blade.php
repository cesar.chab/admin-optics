<div class="modal fade modal-slide-in-right" id="modal-delete-{{$patient->id}}" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				@if(empty($patient->deleted_at))
				<h5 class="modal-title">Baja paciente</h5>
				@else
				<h5 class="modal-title">Activar paciente</h5>
				@endif
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<p>{{empty($patients->deleted_at) ? 'Al dar de baja al paciente ya no podra seguir evolucionando en el sistema y solo se podra consultar el historico' : 'Al activar el paciente podra continuar con evolución al sistema'}}</p>
				<p>Desea continuar ?</p>
			</div>
			<div class="modal-footer">
				{{Form::Open(array('action'=>array('PatientController@destroy',$patient->id),'method'=>'delete'))}}
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				@if(empty($patient->deleted_at))
				<button type="submit" class="btn btn-danger">Baja</button>
				@else
				<button type="submit" class="btn btn-success">Activar</button>
				@endif
				{{Form::Close()}}
			</div>
		</div>
	</div>
</div>
@if (isset($history->id))
<div class="modal fade modal-slide-in-right" id="modal-evolution-{{$history->id}}" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">				
				<h5 class="modal-title">Eliminar evolución</h5>				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Al eliminar la evolución ya no seguira visible en la historia del paciente</p>
				<p>Desea continuar ?</p>
			</div>
			<div class="modal-footer">
				{{Form::Open(array('id' => 'deleteHistory' ,'action'=>array('HistoryController@destroy',$history->id),'method'=>'delete'))}}
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>				
				<button type="submit" class="btn btn-danger">Eliminar</button>								
				{{Form::Close()}}
			</div>
		</div>
	</div>
</div>
@endif