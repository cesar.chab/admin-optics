@extends('layouts.app', ['activePage' => 'patients', 'titlePage' => 'Evolución paciente'])
@section('css')
<link href="{{asset('css/material-pro.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Evolución</h4>
                        <p class="card-category">Historia del paciente {{$patient->full_name}}</p>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span>{{ session('status') }}</span>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12 col-xs-12 _text-right">
                                @if ($patient->deleted_at)
                                <div class="alert alert-warning" data-dismiss="alert" aria-label="Close" class="close">
                                    <button type="button" class="close" aria-label="Close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span>
                                        <b> Advertencia - </b> El paciente actualmente esta dado de baja, no puede seguir evolucionando!</span>
                                </div>
                                @endif
                                @if (!$patient->deleted_at)
                                <a rel="tooltip" class="btn btn-primary" href="" data-target="#modal-history-{{$patient->id}}" data-toggle="modal" title="Evolución">
                                    Agregar Historia
                                </a>
                                @endif
                                <a class="btn btn-danger float-right_" href="{{route('patient.index')}}">Volver a la lista</a>
                                @include('patients.modal-history')
                            </div>
                        </div>
                        <div class="card card-timeline card-plain">
                            <div class="card-body">
                                <ul class="timeline">
                                    @foreach($patient->medicalHistories as $key => $history)
                                    <li class="{{($key % 2 == 0) ? 'timeline-inverted' : ''}}">
                                        <div class="timeline-badge {{($key % 2 == 0) ? 'primary' : 'success'}}">
                                            <i class="material-icons">{{($key % 2 == 0) ? 'schedule' : 'history'}}</i>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-heading">
                                                <span class="badge badge-pill badge-{{($key % 2 == 0) ? 'primary' : 'success'}}">{{!empty($history->title) ? $history->title . ' / ' . $history->updated_at : $history->updated_at}}</span>
                                            </div>
                                            <div class="timeline-body">
                                                <p>
                                                    <strong>OD: </strong> {{$history->right_eye}}
                                                </p>
                                                <p>
                                                    <strong>OI: </strong> {{$history->left_eye}}
                                                </p>
                                                <p>
                                                    <strong>AD: </strong> {{$history->right_rise}}
                                                </p>
                                                <p>
                                                    <strong>DI: </strong> {{$history->left_rise}}
                                                </p>
                                                <p>
                                                    <strong>Tipo lente: </strong> {{$history->lens_type}}
                                                </p>
                                                <p><strong>Observaciones:</strong></p>
                                                <p> {{$history->comments}} </p>
                                            </div>
                                            <div class="timeline-footer">
                                                <small>
                                                    <strong>Autor:</strong> {{$history->user->name}}
                                                </small>
                                                <small><br>
                                                    {{getDateColumn($history, 'updated_at')}}
                                                </small><br>
                                                <a rel="tooltip" class="btn btn-danger btn-link" href="" data-target="#modal-evolution-{{$history->id}}" data-toggle="modal" title="Eliminar evolución">
                                                    <i class="material-icons">close</i> Eliminar
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                    @include('patients.modal')
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection