@extends('layouts.app', ['activePage' => 'patients', 'titlePage' => 'Editar paciente'])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- <form method="post" action="{{route('patient.update', $patient->id)}}" autocomplete="off" enctype="multipart/form-data" class="form-horizontal"> -->
                <form method="post" action="{{route('patient.update', $patient->id)}}" autocomplete="off" enctype="multipart/form-data" class="form-horizontal">
                    @csrf
                    @method('put')
                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <p class="card-category">Informacion del paciente</p>
                        </div>
                        <div class="card-body">
                            @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-8 col-xs-12">
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Nombre</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="Nombre" value="{{ $patient->name }}" required="true" aria-required="true" />
                                                @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Apellido Paterno</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" id="input-last-name" type="text" placeholder="Apellido paterno" value="{{ $patient->last_name }}" required="true" aria-required="true" />
                                                @if ($errors->has('last_name'))
                                                <span id="last-name-error" class="error text-danger" for="input-last-name">{{ $errors->first('last_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Apellido Materno</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('mother_last_name') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('mother_last_name') ? ' is-invalid' : '' }}" name="mother_last_name" id="input-mother-last-name" type="text" placeholder="Apellido materno" value="{{ $patient->mother_last_name }}" required="true" aria-required="true" />
                                                @if ($errors->has('mother_last_name'))
                                                <span id="mother-last-name-error" class="error text-danger" for="input-mother-last-name">{{ $errors->first('mother_last_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <label class="col-sm-2 col-form-label">Edad</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('age') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('age') ? ' is-invalid' : '' }}" name="age" id="input-age" type="number" placeholder="Edad" value="{{ $patient->age }}" />
                                            @if ($errors->has('age'))
                                            <span id="email-error" class="error text-danger" for="input-age">{{ $errors->first('age') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                    <!-- <div class="row">
                                        <label class="col-sm-2 col-form-label">Fecha nacimiento</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('birthday') ? ' has-danger' : '' }}">
                                                <input class="form-control" name="birthday" id="input-birthday" type="date" placeholder="Fecha nacimiento" value="{{ $patient->birthday }}" />
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Correo</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="Correo" value="{{ $patient->email }}" />
                                                @if ($errors->has('email'))
                                                <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Teléfono</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" id="input-phone" type="tel" maxlength="10" minlength="10" placeholder="Teléfono" value="{{ $patient->phone }}"/>
                                                @if ($errors->has('phone'))
                                                <span id="phone-error" class="error text-danger" for="input-phone">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Dirección</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="input-address" type="text" placeholder="Dirección" value="{{ $patient->address }}" />
                                                @if ($errors->has('address'))
                                                <span id="address-error" class="error text-danger" for="input-address">{{ $errors->first('address') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group form-file-upload form-file-multiple">
                                        <input type="file" name="image" id="image" accept="image/*" class="inputFileHidden file">
                                        <div class="input-group">
                                            <input id="file" type="text" class="form-control inputFileVisible" placeholder="Cargar imagen">
                                            <span class="input-group-btn">
                                                <button type="button" class="browse btn btn-fab btn-round btn-primary">
                                                    <i class="material-icons">attach_file</i>
                                                </button>
                                            </span>
                                        </div>
                                        <img id="preview" class="img-thumbnail" src="{{$patient->getFirstMediaUrl('image', 'thumb')}}" alt="{{$patient->name}}">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <a class="btn btn-danger float-right" href="{{route('patient.index')}}">Volver a la lista</a>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
