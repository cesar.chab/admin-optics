<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('material') }}/img/15079.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  @php 
  if (!isset($activePage)) {
	$activePage = '';
  }
  @endphp
  <div class="logo">
    <a href="/home" class="simple-text logo-normal">
      Optica Frandel
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
	  </li>
	  @can('config')
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="true">
          <i class="material-icons">settings</i>
          <p>Configuración
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse show" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('profile.edit') }}">
                <span class="sidebar-mini"> <i class="material-icons">person</i> </span>
                <span class="sidebar-normal">Perfil </span>
              </a>
			</li>
			@can('user.index')
            <li class="nav-item{{ $activePage == 'user' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('user.index') }}">
                <span class="sidebar-mini"> <i class="material-icons">group-add</i> </span>
                <span class="sidebar-normal"> Usuarios </span>
              </a>
			</li>
			@endcan
          </ul>
        </div>
	  </li>
	  @endcan
      <li class="nav-item{{ $activePage == 'patients' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('patient.index') }}">
          <i class="material-icons">accessibility</i>
            <p>Pacientes</p>
        </a>
      </li>
     
    </ul>
  </div>
</div>