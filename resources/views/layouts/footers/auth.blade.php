<footer class="footer">
	<div class="container-fluid">		
		<div class="copyright float-right">
			Copyright © 2020 All rights reserved, made by <a href="https://cesarchabuluac.com" target="_blank">Cesar Chab</a>
		</div>
	</div>
</footer>