
{!! Form::open(array('url'=>'user','method'=>'GET','autocomple'=>'of','role'=>'search')) !!}
<div class="form-group row">
	<div class="col-md-12">
		<div class="input-group">	
            {{--<input type="text" class="form-control border-bottom-0 input-sm br-tr-md-0 br-br-md-0" placeholder="Buscar producto" name="searchText" value="{{$searchText}}" >--}}
            <input class="form-control" name="searchText" id="input-search" type="text" placeholder="Buscar usuario..." value="{{$searchText}}" />			
			<button type="submit" class="btn btn-primary btn-sm">
                <i class="material-icons">search</i>
            </button>
		</div>
	</div>
</div>
{!! Form::close() !!}