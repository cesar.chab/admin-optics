<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('last_name');
			$table->string('mother_last_name');
			$table->string('identification')->nullable();
			$table->date('birthday')->nullable();
			$table->string('email');
			$table->string('phone', 10);
			$table->string('address', 255);
            $table->text('comments', 255)->nullable();            
            $table->softDeletes(); 
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
