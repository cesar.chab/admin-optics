<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldMedicalHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_histories', function (Blueprint $table) {
            $table->string('lens_type', 100)->nullable();
            $table->string('right_eye', 100)->nullable()->comment('OD');
            $table->string('left_eye', 100)->nullable()->comment('OI');           
            $table->string('right_rise', 100)->nullable()->comment('AD');
            $table->string('left_rise', 100)->nullable()->comment('DI');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_histories', function (Blueprint $table) {
            $table->dropIfExists('lens_type');
            $table->dropIfExists('right_eye');
            $table->dropIfExists('left_eye');
            $table->dropIfExists('right_rise');
            $table->dropIfExists('left_rise');
        });
    }
}
