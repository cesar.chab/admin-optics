<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;
use Spatie\Permission\Exceptions\RoleDoesNotExist;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
     //$ php artisan db:seed --class=PermissionsTableSeeder
     private $exceptNames = [
        'LaravelInstaller*',
        'LaravelUpdater*',
        'debugbar*',
        'cashier.*'
    ];

    private $exceptControllers = [
        'LoginController',
        'ForgotPasswordController',
        'ResetPasswordController',
        'RegisterController',
        'PayPalController'
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {      
        DB::table('permissions')->delete();
        DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'config',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'user',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            2 => 
            array (
                'id' => 3,
                'name' => 'user.profile',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ),       
            3 => 
            array (
                'id' => 4,
                'name' => 'user.index',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ),  
            4 => 
            array (
                'id' => 5,
                'name' => 'user.create',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ),  
            5 => 
            array (
                'id' => 6,
                'name' => 'user.store',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            6 => 
            array (
                'id' => 7,
                'name' => 'user.show',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ),  
            7 => 
            array (
                'id' => 8,
                'name' => 'user.edit',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            8 => 
            array (
                'id' => 9,
                'name' => 'user.update',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            9 => 
            array (
                'id' => 10,
                'name' => 'user.destroy',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            10 => 
            array (
                'id' => 11,
                'name' => 'patient',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            11 => 
            array (
                'id' => 12,
                'name' => 'patient.index',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            12 => 
            array (
                'id' => 13,
                'name' => 'patient.store',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            13 => 
            array (
                'id' => 14,
                'name' => 'patient.show',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            14 => 
            array (
                'id' => 15,
                'name' => 'patient.edit',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            15 => 
            array (
                'id' => 16,
                'name' => 'patient.update',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            16 => 
            array (
                'id' => 17,
                'name' => 'patient.destroy',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            17 => 
            array (
                'id' => 18,
                'name' => 'user.active',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            18 => 
            array (
                'id' => 19,
                'name' => 'patient.evolution',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
            19 => 
            array (
                'id' => 20,
                'name' => 'patient.active',
                'guard_name' => 'web',                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),                
            ), 
                                                                   
        ));
        /*$routeCollection = Route::getRoutes();
        try{
            $role = Role::findByName('admin');
            if (!$role) {
                $role = Role::create(['name' => 'admin']);
            }
        }catch (Exception $e){
            if($e instanceof RoleDoesNotExist){
                $role = Role::create(['name' => 'admin']);
            }
        }
        foreach ($routeCollection as $route) {
            if ($this->match($route)) {
                // PermissionDoesNotExist
                try{
                    if(!$role->hasPermissionTo($route->getName())){
                        $permission = Permission::create(['name' => $route->getName()]);
                        $role->givePermissionTo($permission);
                    }
                }catch (Exception $e){
                    if($e instanceof PermissionDoesNotExist){
                        $permission = Permission::create(['name' => $route->getName()]);
                        $role->givePermissionTo($permission);
                    }
                }
            }
        }
        $user = User::find(1);
        if(!$user->hasRole('admin')){
            $user->assignRole('admin');
        }*/
    }

    private function match(Illuminate\Routing\Route $route)
    {
        if ($route->getName() === null) {
            return false;
        } else {
            if(preg_match('/API/',class_basename($route->getController()))){
                return false;
            }
            if (in_array(class_basename($route->getController()), $this->exceptControllers)) {
                return false;
            }
            foreach ($this->exceptNames as $except) {
                if (str_is($except, $route->getName())) {
                    return false;
                }
            }
        }
        return true;
    }
}
