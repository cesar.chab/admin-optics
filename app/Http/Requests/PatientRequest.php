<?php

namespace App\Http\Requests;

use App\Patient;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PatientRequest extends FormRequest
{
    protected $redirectRoute = 'patient.create'; //ruta definida en alguno de los archivos de la carpeta routes/
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required', 'min:3'
            ],
            // 'last_name' => [
            //     'required', 'min:3'
            // ],            
            // 'email' => [
            //     'required', 'email', Rule::unique((new Patient)->getTable())->ignore($this->route()->patient->id ?? null)
            // ],
            // 'phone' => [
            //     'required', 'min:10:numeric'
            // ],
            // 'address' => [
            //     'required', 'min:3'
            // ]
            // 'age' => [
            //     'required', 'integer', 'min:0' , 'not_in:0'
            // ]

        ];
    }

    // public function messages()
    // {
    //     return [
    //         'name.required' => 'El :attribute es obligatorio.',
    //         'last_name.required' => 'El :attribute es obligatorio',
    //         'email.required' => 'El :attribute es obligatorio',
    //         'phone.required' => 'El :attribute es obligatorio',
    //         'address.required' => 'El :attribute es obligatorio',
    //     ];
    // }

    // public function attributes()
    // {
    //     return [
    //         'name' => 'nombre',
    //         'last_name' => 'apellido paterno',
    //         'email' => 'correo',
    //         'phone' => 'teléfono',
    //         'address' => 'dirección'
    //     ];
    // }
}
