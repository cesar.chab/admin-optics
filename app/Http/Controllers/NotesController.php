<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    public function index()
    {
        $note = Note::all();
        return view('notes.index', ['notes' => $note]);
    }

    public function destroy($id)
    {
        Note::findOrFail($id)->delete();

        return redirect()->back();
    }
}
