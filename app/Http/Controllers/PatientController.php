<?php

namespace App\Http\Controllers;

use App\Http\Requests\PatientRequest;
use App\Patient;
use App\Upload;
use Exception;
use Illuminate\Contracts\Session\Session as SessionSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Prettus\Validator\Exceptions\ValidatorException;

class PatientController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $search = trim($request->get('searchText'));
            $patients = Patient::withTrashed()->where('name', 'LIKE', '%' . $search . '%')
                ->orWhere('last_name', 'LIKE', '%' . $search . '%')
                ->orWhere('mother_last_name', 'LIKE', '%' . $search . '%')
                ->orWhere('phone', 'LIKE', '%' . $search . '%')
                ->orWhere('email', 'LIKE', '%' . $search . '%')
                ->orWhere('full_name', 'LIKE', '%' . $search . '%')
                ->orderBy('last_name', 'ASC')
                ->orderBy('mother_last_name', 'ASC')
                ->paginate(10);
            return view('patients.index', ['patients' => $patients, 'searchText' => $search]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patients.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientRequest $request)
    {
        try {
            $patient = new Patient();
            $input = $request->all();
            $patient->name = $input['name'];
            $patient->last_name = $input['last_name'];
            $patient->mother_last_name = $input['mother_last_name'];
            // $patient->birthday = $input['birthday'];
            $patient->age = $input['age'];
            $patient->email = $input['email'];
            $patient->phone = $input['phone'];
            $patient->address = $input['address'];

            if (isset($input['image']) && $input['image']) {
                $patient->addMediaFromRequest('image')->toMediaCollection('image');
            }
            $patient->save();

            Session::flash('status', 'Paciente creado exitosamente');
        } catch (Exception $e) {
            //Flash::error($e->getMessage());
            Session::flash('status', 'Error generado ' . $e->getMessage());
            //return back()->withErrors('your error message');

        }

        return redirect('patient');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient = Patient::withTrashed()->where('id', $id)->first();
        return view('patients.evolution', ['patient' => $patient]);
    }

    public function evolution ($id) {
        $patient = Patient::find($id);
        return view('patients.evolution', ['patient' => $patient]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = Patient::find($id);
        return view('patients.edit', ['patient' => $patient]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatientRequest $request, $id)
    {
        try {
            $patient = Patient::find($id);
            $input = $request->all();
            $patient->name = $input['name'];
            $patient->last_name = $input['last_name'];
            $patient->mother_last_name = $input['mother_last_name'];
            // $patient->birthday = $input['birthday'];
            $patient->age = $input['age'];
            $patient->email = $input['email'];
            $patient->phone = $input['phone'];
            $patient->address = $input['address'];

            if (isset($input['image']) && $input['image']) {
                $patient->addMediaFromRequest('image')->toMediaCollection('image');
            }
            $patient->update($input);

            Session::flash('status', 'Paciente actualizado exitosamente');
        } catch (Exception $e) {            
            Session::flash('status', 'Error generado ' . $e->getMessage());            
        }

        return redirect('patient');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::withTrashed()->where('id', $id)->first();        
        if (empty($patient)) {
            Session::flash('status', 'Paciente no encontrado');
            return redirect(route('patient.index'));
        }

        if (empty($patient->deleted_at)) {
            $patient->delete($id);
            Session::flash('status', 'Paciente dado de baja exitosamente');
        } else {         
            $patient->restore();
            Session::flash('status', 'paciente activado exitosamente');
        }        
        
        return redirect(route('patient.index'));
    }
}
