<?php

namespace App\Http\Controllers;

use App\Patient;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $patient = Patient::all()->count();
        $user = User::all()->count();
        return view('dashboard', ['patient' => $patient, 'user' => $user]);
    }
}
