<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // $user = User::find(6);        
        // echo "<pre>";
        // print_r($user->getMedia('avatars')->last()->getFullUrl());
        // // print_r($user->avatar());
        // // print_r($user->getMedia('avatars')->last());
        // // print_r($user->getMedia('avatars')->last());
        // exit();
        if ($request) {
            $search = trim($request->get('searchText'));
            $users = User::withTrashed()->where('name', 'LIKE', '%' . $search . '%')
                ->orWhere('email', 'LIKE', '%' . $search . '%')
                ->orderBy('id', 'DESC')
                ->paginate(10);
            return view('users.index', ['users' => $users, 'searchText' => $search]);
        }
    }

    public function create()
    {
        $role = Role::all()->pluck('name', 'name');        
        return view('users.add', ['role' => $role]);
    }

    public function search(Request $request)
    {
        $filter = $request->search;
        $users = User::where('name', 'LIKE', '%' . $filter . '%')
            ->orWhere('email', 'LIKE', '%' . $filter . '%')
            ->orderBy('id', 'DESC')
            ->paginate(10);
        return [
            'pagination' => [
                'total' => $users->total(),
                'current_page' => $users->currentPage(),
                'per_page' => $users->perPage(),
                'last_page' => $users->lastPage(),
                'from' => $users->firstItem(),
                'to' => $users->lastItem()
            ],
            'users' => $users
        ];
    }

    public function store(UserRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        if (isset($request->avatar) && $request->avatar) {
            $user->addMediaFromRequest('avatar')->toMediaCollection('avatars');
        }
        $user->save();
        $user->assignRole($request->role_id);
        
        Session::flash('status', 'Usuario creado exitosamente');
        return redirect('user');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all()->pluck('id', 'name');             
        $rolesSelected = $user->roles()->pluck('id')->toArray();
        return view('users.edit', ['user' => $user, 'roles' => $roles, 'rolesSelected' => $rolesSelected]);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::find($id);       
        $input = $request->all();        
        if (empty($input['password'])) {
            unset($input['password']);
        } else {
            $input['password'] = Hash::make($input['password']);
        }       
        $user->update($input);

        if (isset($input['avatar']) && $request['avatar']) {
           // $user->clearMediaCollection();
            $user->addMediaFromRequest('avatar')->toMediaCollection('avatars');
        }
        $user->syncRoles($input['role_id']);

        if (auth()->user()->id == $id) {
            if (!empty($input['password'])) {
                Auth::logout();
                redirect('/');
            }
        }
        Session::flash('status', 'Usuario actualizado exitosamente');
        return redirect('user');
    }

    public function destroy($id) {
        $user = User::withTrashed()->where('id', $id)->first();        
        if (empty($user)) {
            Session::flash('status', 'Usuario no encontrado');
            return redirect(route('user.index'));
        }

        if (empty($user->deleted_at)) {
            $user->delete($id);
            Session::flash('status', 'Usuario bloqueado exitosamente');
        } else {         
            $user->restore();
            Session::flash('status', 'Usuario activado exitosamente');
        }        
        
        return redirect(route('user.index'));
    }
}
