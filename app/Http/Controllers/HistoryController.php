<?php

namespace App\Http\Controllers;

use App\Http\Requests\MedicalHistoryRequest;
use App\MedicalHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicalHistoryRequest $request)
    {       
        $history = new MedicalHistory();
        $input = $request->all();        
        $input['user_id'] = Auth::user()->id;
        $history->create($input);        
        Session::flash('status', 'Historia guardada exitosamente');
        return back(); //view('patients.evolution', ['patient' => $patient]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $history = MedicalHistory::find($id);
        if (empty($history)) {
            Session::flash('status', 'Historia paciente no encontrado');
            return back(); 
        }

        $history->delete($id);
        Session::flash('status', 'Historia paciente eliminado exitosamente');           
        return back(); 
    }
}
