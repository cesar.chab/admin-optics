<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Patient extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait {
        getFirstMediaUrl as protected getFirstMediaUrlTrait;
    }

    protected $fillable = [
        'name',
        'last_name',
        'mother_last_name',
        'email',
        'phone',
        'address'
    ];

    protected $appends = [
        'full_name',
        'has_media'
    ];

    public function getFullName()
    {
        return $this->name . ' ' . $this->last_name . ' ' . $this->mother_last_name;
    }

    public function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->last_name . ' ' . $this->mother_last_name;
    }

    public function medicalHistories()
    {
        return $this->hasMany('App\MedicalHistory')->orderBy('created_at', 'ASC');
    }

    /**
     * @param Media|null $media
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 200, 200)
            ->sharpen(10);

        $this->addMediaConversion('icon')
            ->fit(Manipulations::FIT_CROP, 100, 100)
            ->sharpen(10);
    }

    /**
     * to generate media url in case of fallback will
     * return the file type icon
     * @param string $conversion
     * @return string url
     */
    public function getFirstMediaUrl($collectionName = 'default', $conversion = '')
    {
        $url = $this->getFirstMediaUrlTrait($collectionName);
        if ($url) {
            $array = explode('.', $url);
            $extension = strtolower(end($array));
            if (in_array($extension, config('medialibrary.extensions_has_thumb'))) {
                return asset($this->getFirstMediaUrlTrait($collectionName, $conversion));
            } else {
                return asset(config('images/icons') . '/' . $extension . '.png');
            }
        } else {
            return asset('avatar/default.png');
        }        
    }

    /**
     * Add Media to api results
     * @return bool
     */
    public function getHasMediaAttribute()
    {
        return $this->hasMedia('image') ? true : false;
    }
}
