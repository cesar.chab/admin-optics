<?php


use InfyOm\Generator\Common\GeneratorCuisine;
use InfyOm\Generator\Utils\GeneratorCuisinesInputUtil;
use InfyOm\Generator\Utils\HTMLCuisineGenerator;
use Symfony\Component\Debug\Exception\FatalThrowableError;


/**
 * @param $bytes
 * @param int $precision
 * @return string
 */
function formatedSize($bytes, $precision = 1)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    $bytes /= pow(1024, $pow);

    return round($bytes, $precision) . ' ' . $units[$pow];
}

function getMediaColumn($mediaModel, $mediaCollectionName = '', $extraClass = '', $mediaThumbnail = 'icon')
{
    $extraClass = $extraClass == '' ? ' rounded ' : $extraClass;

    if ($mediaModel->hasMedia($mediaCollectionName)) {
        return "<img class='" . $extraClass . "' style='width:50px' src='" . $mediaModel->getFirstMediaUrl($mediaCollectionName, $mediaThumbnail) . "' alt='" . $mediaModel->getFirstMedia($mediaCollectionName)->name . "'>";
    }else{
        return "<img class='" . $extraClass . "' style='width:50px' src='" . asset('images/image_default.png') . "' alt='image_default'>";
    }
}

function getDateColumn($modelObject, $attributeName = 'updated_at')
{
    setlocale(LC_ALL,"es_ES");
    if (config('is_human_date_format', false)) {
        $html = '${dateHuman}';
    } else {
        $html = '${date}';
    }
    if (!isset($modelObject[$attributeName])) {
        return '';
    }
    
    $dateObj = new Carbon\Carbon($modelObject[$attributeName]);
    $replace = preg_replace('/\$\{date\}/', $dateObj->format(config('date_format', 'l jS F Y (h:i:s)')), $html);
    $replace = preg_replace('/\$\{dateHuman\}/', $dateObj->diffForHumans(), $replace);
    return $replace;
}