<?php

namespace App\Repositories;


use App\Media;
use App\Patient;
use App\Upload;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
// use InfyOm\Generator\Common\BaseRepository;
//use Your Model

/**
 * Class PatientRepository.
 */
class PatientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

      /**
     * Configure the Model
     **/
    public function model()
    {
        return Patient::class;
    }
}
